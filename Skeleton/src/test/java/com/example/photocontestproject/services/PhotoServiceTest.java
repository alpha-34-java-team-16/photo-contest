package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Photo;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.repositories.contracts.PhotoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.photocontestproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceTest {

    @Mock
    PhotoRepository mockRepository;

    @InjectMocks
    PhotoServiceImpl photoService;

    @Test
    public void getAll_Should_ReturnAllPhotos_When_PhotoExist() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        photoService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Throw_When_PhotoDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenThrow(EntityNotFoundException.class);

        //Act
        photoService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnPhoto_When_PhotoExist() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        Mockito.when(mockRepository.getById(mockPhoto.getId())).thenReturn(null);

        //Act
        photoService.getById(mockPhoto.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(mockPhoto.getId());
    }

    @Test
    public void getById_Should_Throw_When_PhotoDoesNotExist() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        Mockito.when(mockRepository.getById(mockPhoto.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> photoService.getById(mockPhoto.getId()));
    }

    @Test
    public void getByPath_Should_ReturnPhoto_When_PhotoExist() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        Mockito.when(mockRepository.getByField("path", mockPhoto.getPath())).thenReturn(null);

        //Act
        photoService.getByPath(mockPhoto.getPath());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByField("path", mockPhoto.getPath());
    }

    @Test
    public void getByPath_Should_Throw_When_PhotoDoesNotExist() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        Mockito.when(mockRepository.getByField("path", mockPhoto.getPath()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> photoService.getByPath(mockPhoto.getPath()));
    }

    @Test
    public void getCoverPhotos_Should_ReturnPhoto_When_PhotoExist() {
        //Arrange
        Mockito.when(mockRepository.getCoverPhotos()).thenReturn(null);

        //Act
        photoService.getCoverPhotos();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getCoverPhotos();
    }

    @Test
    public void getCoverPhotos_Should_Throw_When_PhotoDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getCoverPhotos())
                .thenThrow(EntityNotFoundException.class);

        //Act
        photoService.getCoverPhotos();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getCoverPhotos();
    }

    @Test
    public void createPhoto_Should_CallRepository_When_PhotoWithSamePathDoesNotExists() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        Mockito.when(mockRepository.getByField("path", mockPhoto.getPath()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        photoService.create(mockPhoto);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockPhoto);
    }

    @Test
    public void createPhoto_Should_NotCallRepository_When_PhotoWithSamePathExists() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        Photo mockPhoto2 = createMockPhotoCoverFile();
        Mockito.when(mockRepository.getByField("path", mockPhoto.getPath()))
                .thenReturn(mockPhoto);

        //Act
        photoService.create(mockPhoto);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(0)).create(mockPhoto2);
    }

    @Test
    public void updatePhoto_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        User mockUser = createMockUser();

        //Act, Assert
        Mockito.verify(mockRepository, Mockito.times(0)).update(mockPhoto);
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> photoService.update(mockPhoto, mockUser));
    }

    @Test
    public void updatePhoto_Should_Throw_When_PhotoIdDoesNotExist() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getById(mockPhoto.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> photoService.update(mockPhoto, mockOrganizer));
    }

    @Test
    public void updatePhoto_Should_CallRepository() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getById(mockPhoto.getId()))
                .thenReturn(mockPhoto);

        //Act
        photoService.update(mockPhoto, mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getById(mockPhoto.getId());
        Mockito.verify(mockRepository, Mockito.times(1)).update(mockPhoto);
    }

    @Test
    public void deletePhoto_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        User mockUser = createMockUser();

        //Act, Assert
        Mockito.verify(mockRepository, Mockito.times(0)).delete(mockPhoto.getId());
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> photoService.delete(mockPhoto.getId(), mockUser));
    }

    @Test
    public void deletePhoto_Should_CallRepository() {
        //Arrange
        Photo mockPhoto = createMockPhotoCoverFile();
        User mockOrganizer = createMockOrganizer();

        //Act
        photoService.delete(mockPhoto.getId(), mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).delete(mockPhoto.getId());
    }

}
