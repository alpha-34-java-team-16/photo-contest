package com.example.photocontestproject.exceptions;

public class IllegalRequestException extends RuntimeException {

    public IllegalRequestException(String message) {
        super(message);
    }

    public IllegalRequestException() {

    }
}
