package com.example.photocontestproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "reviews")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id")
    private int id;

    @Column(name = "score")
    private int score;

    @Column(name = "comment")
    private String comment;

    @OneToOne
    @JoinColumn(name = "juror_id")
    private User juror;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "entry_id")
    private Entry entry;

    @Column(name = "is_wrong_category")
    private boolean isWrongCategory;

    public Review() {
    }

    public Review(int id, int score, String comment, User juror, boolean isWrongCategory) {
        this.id = id;
        this.score = score;
        this.comment = comment;
        this.juror = juror;
        this.isWrongCategory = isWrongCategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getJuror() {
        return juror;
    }

    public void setJuror(User juror) {
        this.juror = juror;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public boolean isWrongCategory() {
        return isWrongCategory;
    }

    public void setIsWrongCategory(boolean correctCategory) {
        this.isWrongCategory = correctCategory;
    }
}
