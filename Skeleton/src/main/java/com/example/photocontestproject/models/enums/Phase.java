package com.example.photocontestproject.models.enums;

public enum Phase {

    FIRST,
    SECOND,
    FINISHED;

    @Override
    public String toString() {
        switch (this) {
            case FIRST:
                return "First";
            case SECOND:
                return "Second";
            case FINISHED:
                return "Finished";
            default:
                throw new IllegalArgumentException();
        }
    }
}
