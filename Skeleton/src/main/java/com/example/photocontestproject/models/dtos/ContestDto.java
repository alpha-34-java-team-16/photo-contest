package com.example.photocontestproject.models.dtos;

import org.hibernate.validator.constraints.UniqueElements;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContestDto {

    private int id;

    @NotEmpty(message = "Title field cannot be empty")
    @Size(min = 4, max = 500, message = "Title must between 4 and 32 symbols long")
    private String title;

    @NotEmpty(message = "Title field cannot be empty")
    private String categoryName;

    @NotEmpty
    private String openAccess;

    @NotNull
    private int phaseOneDays;

    @NotNull
    private int phaseTwoHours;

    private Integer[] additionalJury;

    private String photoUrl;

    public ContestDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getOpenAccess() {
        return openAccess;
    }

    public void setOpenAccess(String openAccess) {
        this.openAccess = openAccess;
    }

    public int getPhaseOneDays() {
        return phaseOneDays;
    }

    public void setPhaseOneDays(int phaseOneDays) {
        this.phaseOneDays = phaseOneDays;
    }

    public int getPhaseTwoHours() {
        return phaseTwoHours;
    }

    public void setPhaseTwoHours(int phaseTwoHours) {
        this.phaseTwoHours = phaseTwoHours;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer[] getAdditionalJury() {
        return additionalJury;
    }

    public void setAdditionalJury(Integer[] additionalJury) {
        this.additionalJury = additionalJury;
    }
}
