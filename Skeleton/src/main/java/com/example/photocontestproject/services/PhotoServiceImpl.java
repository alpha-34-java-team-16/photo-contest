package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Photo;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.repositories.contracts.PhotoRepository;
import com.example.photocontestproject.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;


@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }


    @Override
    public Photo getById(int id) {
        try {
            return photoRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public Photo getByPath(String path) {
        try {
            return photoRepository.getByField("path", path);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<Photo> getCoverPhotos() {
        try {
            return photoRepository.getCoverPhotos();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Photo> getAll() {
        try {
            return photoRepository.getAll();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public void create(Photo photo) {
        boolean duplicateExists = true;
        try {
            getByPath(photo.getPath());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (!duplicateExists) {
            photoRepository.create(photo);
        }
    }

    @Override
    public void update(Photo photo, User loggedUser) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_CREATE_CONTESTS);
        }
        try {
            photoRepository.getById(photo.getId());
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        photoRepository.update(photo);
    }

    @Override
    public void delete(int id, User loggedUser) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_CREATE_CONTESTS);
        }
        photoRepository.delete(id);
    }

    @Override
    public void writeFile(MultipartFile file, boolean isCover) throws IOException {
        byte[] bytes = file.getBytes();
        Path path;
        if (isCover) {
           path = Paths.get(UPLOADED_FOLDER_COVER_PHOTO + file.getOriginalFilename());
        } else {
            path =  Paths.get(UPLOADED_FOLDER_ENTRY_PHOTO + file.getOriginalFilename());
        }
        Files.write(path, bytes);
    }
}
