package com.example.photocontestproject.repositories.contracts;

import com.example.photocontestproject.models.Role;

public interface RoleRepository extends BaseReadRepository<Role> {

    Role getByRole(String role);
}
