package com.example.photocontestproject.repositories.contracts;

import com.example.photocontestproject.models.Photo;

import java.util.List;

public interface PhotoRepository extends BaseCRUDRepository<Photo> {
    List<Photo> getCoverPhotos();
}
