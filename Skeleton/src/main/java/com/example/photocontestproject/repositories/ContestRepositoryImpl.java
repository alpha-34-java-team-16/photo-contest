package com.example.photocontestproject.repositories;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;

@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepository<Contest> implements ContestRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        super(Contest.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Contest> getOpenByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("select distinct c from Contest c "
                    + "join c.entries e where (c.phase = :phaseF or c.phase = :phaseS)  " +
                    "and (e.participant.id = :id) order by c.creationTime desc ", Contest.class);
            query.setParameter("phaseF", Phase.FIRST);
            query.setParameter("phaseS", Phase.SECOND);
            query.setParameter("id", user.getId());
            List<Contest> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(CONTEST_UPPERCASE, USER_LOWERCASE, String.valueOf(user.getId()));
            }
            return result;
        }
    }

    @Override
    public int getCountAllContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(*) from Contest e", Long.class);
            return Integer.parseInt(String.valueOf(query.uniqueResult()));
        }
    }


    @Override
    public List<Contest> getFinishedByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("select distinct c from Contest c "
                    + "join c.entries e where (c.phase = :phaseF) " +
                    "and (e.participant.id = :id) order by c.creationTime desc ", Contest.class);
            query.setParameter("phaseF", Phase.FINISHED);
            query.setParameter("id", user.getId());
            List<Contest> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(CONTEST_UPPERCASE, USER_LOWERCASE, String.valueOf(user.getId()));
            }
            return result;
        }
    }

    @Override
    public List<Contest> getAllNotFinished() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    "from Contest where phase = :phaseF or phase = :phaseS order by creationTime desc", Contest.class);
            query.setParameter("phaseF", Phase.FIRST);
            query.setParameter("phaseS", Phase.SECOND);

            List<Contest> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(CONTEST_UPPERCASE, PHASE_LOWERCASE, Phase.FIRST.toString());
            }
            return result;
        }
    }

    @Override
    public List<Contest> getAllInPhaseWithOrWithoutInvites(int userId, Phase phase, int maxResults) {
        try (Session session = sessionFactory.openSession()) {
            String queryString;
            if (userId != 0) {
                queryString = "select distinct c from Contest c " +
                        "left join c.invites i where c.phase = :phase " +
                        "and (c.openAccess = true or i.receiver.id = :id ) order by c.creationTime desc ";
            } else {
                queryString = "select distinct c from Contest c where c.phase = :phase order by c.creationTime desc ";
            }
            Query<Contest> query = session.createQuery( queryString, Contest.class);
            query.setParameter("phase",phase);
            if (userId != 0) query.setParameter("id",userId);
            if(maxResults == 6) {
                query.setMaxResults(6);
            }
            List<Contest> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Contest", "phase", String.valueOf(phase));
            }
            return result;
        }
    }

    @Override
    public List<Contest> getMostRecentlyFinished() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    "from Contest c where phase = :phaseFi and c.entries.size > 0 order by creationTime desc", Contest.class);
            query.setParameter("phaseFi", Phase.FINISHED);
            query.setMaxResults(1);
            List<Contest> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(CONTEST_UPPERCASE, PHASE_LOWERCASE, Phase.FINISHED.toString());
            }
            return result;
        }
    }

    @Override
    public List<Contest> filter(Optional<String> title, Optional<String> openAccess,
                                Optional<Phase> phase, Optional<Integer> categoryId) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from Contest ");
            var queryParams = new HashMap<String, Object>();
            var filter = new ArrayList<String>();

            title.ifPresent(value -> {
                filter.add(" title like: title ");
                queryParams.put("title", "%" + value + "%");
            });

            openAccess.ifPresent(value -> {
                filter.add(" open_access like :openAccess ");
                queryParams.put("openAccess", value);
            });

            phase.ifPresent(value -> {
                filter.add(" phase like :phase ");
                queryParams.put("phase", value);
            });

            categoryId.ifPresent(value -> {
                filter.add(" category.id = :categoryId ");
                queryParams.put("categoryId", value);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            Query<Contest> queryList = session.createQuery(queryString.toString(), Contest.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }
}

