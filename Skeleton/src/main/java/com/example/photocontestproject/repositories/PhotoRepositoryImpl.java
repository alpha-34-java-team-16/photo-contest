package com.example.photocontestproject.repositories;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Photo;
import com.example.photocontestproject.repositories.contracts.PhotoRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhotoRepositoryImpl extends AbstractCRUDRepository<Photo> implements PhotoRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(Photo.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Photo> getCoverPhotos() {
        try (Session session = sessionFactory.openSession()) {
            Query<Photo> query = session.createQuery(
                    "select distinct p from Photo p join Contest c on c.coverPhoto.id = p.id", Photo.class);
            List<Photo> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("No cover photos available");
            }
            return result;
        }
    }
}
