package com.example.photocontestproject.repositories;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Ranking;
import com.example.photocontestproject.models.enums.UserSortOptions;
import com.example.photocontestproject.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAllOrganizers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where role.id = 1", User.class);
            return query.getResultList();
        }
    }

    @Override
    public int getCountAllRegularUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(*) from User e where role.id = 2", Long.class);
            return Integer.parseInt(String.valueOf(query.uniqueResult()));
        }
    }

    @Override
    public List<User> getAvailableUsersForInvite(int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<User> query = session.createNativeQuery(
                    "select * from users u" +
                            " where role_id = 2 and u.user_id not in" +
                            "( select cu.receiver_id from photo.contests_users cu where contest_id = :id)");
            query.setParameter("id", id);
            query.addEntity(User.class);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "contestId", String.valueOf(id));
            }
            return result;
        }
    }

    @Override
    public List<User> getAllEligibleForJury() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where (role.id = 2) and (ranking = 2 or ranking = 3)", User.class);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getContestJury(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<User> query = session.createNativeQuery(
                    "select distinct u.user_id, u.first_name, u.last_name, u.username, u.password, " +
                            "u.email, u.points, u.ranking, u.creation_time, u.role_id " +
                            "from photo.users u " +
                            "join photo.users_contests uc on u.user_id = uc.user_id " +
                            "join photo.contests c on uc.contest_id = c.contest_id where c.contest_id = :id");
            query.setParameter("id", contest.getId());
            query.addEntity(User.class);
            return query.list();
        }
    }

    @Override
    public List<User> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where firstName like :name" +
                    " or lastName like :name or username like :name", User.class);
            query.setParameter("name", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<User> filter(Optional<String> name, Optional<Ranking> ranking, Optional<UserSortOptions> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from User ");
            var queryParams = new HashMap<String, Object>();
            var filter = new ArrayList<String>();

            name.ifPresent(value -> {
                filter.add(" firstName like: name" + " or lastName like :name or username like :name");
                queryParams.put("name", "%" + value + "%");
            });

            ranking.ifPresent(value -> {
                filter.add(" ranking like :ranking");
                queryParams.put("ranking", value);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(value.getQuery());
            });

            Query<User> queryList = session.createQuery(queryString.toString(), User.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    @Override
    public List<Entry> getAllEntries(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery(
                    "from Entry where participant = :user", Entry.class);
            query.setParameter("user", user);
            return query.list();
        }
    }

    @Override
    public List<Invite> getAllInvites(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Invite> query = session.createQuery(
                    "from Invite where receiver = :user", Invite.class);
            query.setParameter("user", user);
            return query.list();
        }
    }
}
