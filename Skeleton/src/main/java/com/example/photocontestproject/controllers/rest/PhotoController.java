package com.example.photocontestproject.controllers.rest;

import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Photo;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.services.contracts.PhotoService;
import com.example.photocontestproject.utils.mappers.PhotoMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.example.photocontestproject.utils.constants.GlobalConstants.PLEASE_SELECT_A_FILE_TO_UPLOAD;
import static com.example.photocontestproject.utils.constants.GlobalConstants.UPLOAD_FOLDER;

@RestController
@RequestMapping("/api/photos")
public class PhotoController {

    private final AuthenticationHelper authenticationHelper;
    private final PhotoService photoService;
    private final PhotoMapper photoMapper;

    public PhotoController(AuthenticationHelper authenticationHelper, PhotoService photoService, PhotoMapper photoMapper) {
        this.authenticationHelper = authenticationHelper;
        this.photoService = photoService;
        this.photoMapper = photoMapper;
    }

    @GetMapping()
    public List<Photo> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return photoService.getAll();
    }

    @GetMapping("/{id}")
    public Photo getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);
        try {
            return photoService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Photo create(@RequestHeader HttpHeaders headers, @RequestParam("file") MultipartFile file) {
        authenticationHelper.tryGetUser(headers);
        if (file.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, PLEASE_SELECT_A_FILE_TO_UPLOAD);
        }
        try {
            Photo photo = photoMapper.dtoToObjectUpload(file);
            photoService.create(photo);
            return photoService.getByPath(UPLOAD_FOLDER + file.getOriginalFilename());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/{id}")
    public Photo update(@PathVariable int id, @RequestHeader HttpHeaders headers,
                        @RequestParam("file") MultipartFile file) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        if (file.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, PLEASE_SELECT_A_FILE_TO_UPLOAD);
        }
        try {
            Photo photo = photoMapper.dtoToObjectUpload(file, id);
            photoService.update(photo, loggedUser);
            return photoService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            photoService.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
